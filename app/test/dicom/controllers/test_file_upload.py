import json
from http import HTTPStatus
from tempfile import NamedTemporaryFile
from unittest import TestCase

from hamcrest import assert_that, is_, not_, none, has_length

from app.main.dicom import create_app
from app.test import test_path

INVALID_DICOM_IMAGE_PATH = f'{test_path}/resources/invalid_dicom_image.dcm'
VALID_DICOM_IMAGE_PATH = f'{test_path}/resources/valid_dicom_image.dcm'


class TestFileUpload(TestCase):

    def setUp(self) -> None:
        app = create_app('test')

        app.testing = True

        self.app = app.test_client()

    def test_returns_http_status_created_when_upload_is_succesful(self):
        with open(VALID_DICOM_IMAGE_PATH, 'rb') as dicom_file:
            data = {'file': dicom_file}

            response = self.app.post('/upload', data=data, content_type='multipart/form-data')

            assert_that(response.status_code, is_(HTTPStatus.CREATED))

    def test_returns_id_when_image_metatada_is_correctly_created_on_database(self):
        with open(VALID_DICOM_IMAGE_PATH, 'rb') as dicom_file:
            data = {'file': dicom_file}

            response = self.app.post('/upload', data=data, content_type='multipart/form-data')

            id = json.loads(response.data.decode('utf-8')).get('id', None)

            assert_that(id, is_(not_(none())))
            assert_that(id, has_length(24))

    def test_returns_http_bad_request_status_when_uploaded_file_is_not_dicom_file(self):
        with NamedTemporaryFile('rb') as not_a_dicom_file:
            data = {'file': not_a_dicom_file}

            response = self.app.post('/upload', data=data, content_type='multipart/form-data')
            response_body = response.json.get('message')

            assert_that(response.status_code, is_(HTTPStatus.BAD_REQUEST))
            assert_that(response_body, is_('Uploaded file must be a DICOM file.'))

    def test_returns_bad_request_http_status_when_dicom_file_is_invalid(self):
        with open(INVALID_DICOM_IMAGE_PATH, 'rb') as dicom_file:
            data = {'file': dicom_file}

            response = self.app.post('/upload', data=data, content_type='multipart/form-data')

            assert_that(response.status_code, is_(HTTPStatus.BAD_REQUEST))
