from http import HTTPStatus
from unittest import TestCase, skip

import pydicom
from hamcrest import assert_that, is_, has_length

from app.main.dicom import create_app
from app.main.dicom.models.ct_image import ComputedTomographyImage
from app.main.dicom.repositories.dicom_repository import DicomRepository
from app.test import test_path

DICOM_IMAGE_PATH = f'{test_path}/resources/valid_dicom_image.dcm'
INVALID_DICOM_METADATA_ID = '5cf44542a75e403be9af9008'


class TestDicomResource(TestCase):
    def setUp(self) -> None:
        app = create_app('test')

        self.app = app.test_client()

        ComputedTomographyImage.objects.delete({})

    def test_retrieve_all_dicom_metadata(self):
        metadata = self.insert_ct_image_on_database()
        first_id = str(metadata.id)

        another_metadata = self.insert_ct_image_on_database()
        another_id = str(another_metadata.id)

        response = self.app.get('/dicom/metadata')

        assert_that(response.status_code, is_(HTTPStatus.OK))

        response_body = response.json

        assert_that(response_body, has_length(2))
        assert_that(response_body[1]['id'], is_(another_id))

        first_metadata = response_body[0]
        assert_that(first_metadata['id'], is_(first_id))

        patient_metadata = first_metadata['patient']
        assert_that(patient_metadata['first_name'], is_(''))
        assert_that(patient_metadata['middle_name'], is_(''))
        assert_that(patient_metadata['family_name'], is_(''))
        assert_that(patient_metadata['sex'], is_(''))

        general_study_metadata = first_metadata['general_study']
        assert_that(general_study_metadata['study_instance_uid'], is_('study_id'))
        assert_that(general_study_metadata['accession_number'], is_(''))

        general_image = first_metadata['general_image']
        assert_that(general_image['instance_number'], is_(1))
        assert_that(general_image['image_type'], is_(['DERIVED', 'SECONDARY']))

        general_series = first_metadata['general_series']
        assert_that(general_series['modality'], is_('CT'))
        assert_that(general_series['series_number'], is_(''))
        assert_that(general_series['series_instance_uuid'], is_('series_id'))

        image_plane = first_metadata['image_plane']
        assert_that(image_plane['pixel_spacing'], is_([0.505859017372131, 0.505859017372131]))
        assert_that(image_plane['image_orientation_patient'], is_([1.0, 0.0, 0.0, 0.0, 1.0, 0.0]))
        assert_that(image_plane['image_position_patient'], is_([1.0, 0.0, 0.0, 0.0, 1.0, 0.0]))

        ct_image = first_metadata['ct_image']
        assert_that(ct_image['bits_allocated'], is_(16))
        assert_that(ct_image['bits_stored'], is_(16))
        assert_that(ct_image['high_bit'], is_(15))
        assert_that(ct_image['rescale_intercept'], is_(0.0))
        assert_that(ct_image['rescale_slope'], is_(1.0))
        assert_that(ct_image['rescale_type'], is_('US'))

        sop_common = first_metadata['sop_common']
        assert_that(sop_common['sop_class_uid'], is_('1.2.840.10008.5.1.4.1.1.2'))
        assert_that(sop_common['sop_instance_uid'],
                    is_('1.2.826.0.1.3680043.2.1125.1.55552443083077366340364975220011362'))
        assert_that(sop_common['instance_number'], is_(1))

    @skip
    def test_retrieve_metadata_by_id(self):
        metadata = self.insert_ct_image_on_database()
        id = str(metadata.id)

        response = self.app.get(f'/dicom/metadata/{id}')

        assert_that(response.status_code, is_(HTTPStatus.OK))
        metadata = response.json
        assert_that(metadata['id'], is_(id))

        patient_metadata = metadata['patient']
        assert_that(patient_metadata['first_name'], is_(''))
        assert_that(patient_metadata['middle_name'], is_(''))
        assert_that(patient_metadata['family_name'], is_(''))
        assert_that(patient_metadata['sex'], is_(''))

        general_study_metadata = metadata['general_study']
        assert_that(general_study_metadata['study_instance_uid'], is_('study_id'))
        assert_that(general_study_metadata['accession_number'], is_(''))

        general_image = metadata['general_image']
        assert_that(general_image['instance_number'], is_(1))
        assert_that(general_image['image_type'], is_(['DERIVED', 'SECONDARY']))

        general_series = metadata['general_series']
        assert_that(general_series['modality'], is_('CT'))
        assert_that(general_series['series_number'], is_(''))
        assert_that(general_series['series_instance_uuid'], is_('series_id'))

        image_plane = metadata['image_plane']
        assert_that(image_plane['pixel_spacing'], is_([0.505859017372131, 0.505859017372131]))
        assert_that(image_plane['image_orientation_patient'], is_([1.0, 0.0, 0.0, 0.0, 1.0, 0.0]))
        assert_that(image_plane['image_position_patient'], is_([1.0, 0.0, 0.0, 0.0, 1.0, 0.0]))

        ct_image = metadata['ct_image']
        assert_that(ct_image['bits_allocated'], is_(16))
        assert_that(ct_image['bits_stored'], is_(16))
        assert_that(ct_image['high_bit'], is_(15))
        assert_that(ct_image['rescale_intercept'], is_(0.0))
        assert_that(ct_image['rescale_slope'], is_(1.0))
        assert_that(ct_image['rescale_type'], is_('US'))

        sop_common = metadata['sop_common']
        assert_that(sop_common['sop_class_uid'], is_('1.2.840.10008.5.1.4.1.1.2'))
        assert_that(sop_common['sop_instance_uid'],
                    is_('1.2.826.0.1.3680043.2.1125.1.55552443083077366340364975220011362'))
        assert_that(sop_common['instance_number'], is_(1))

    def test_returns_not_found_http_status_when_id_does_not_exist(self):
        response = self.app.get(f'/dicom/metadata/{INVALID_DICOM_METADATA_ID}')

        assert_that(response.status_code, is_(HTTPStatus.NOT_FOUND))
        assert_that(response.json['message'], is_(f'Cannot find metadata record with id {INVALID_DICOM_METADATA_ID}'))

    @staticmethod
    def insert_ct_image_on_database():
        with open(DICOM_IMAGE_PATH, 'rb') as dicom_file:
            metadata = pydicom.read_file(dicom_file)

            ct_image = ComputedTomographyImage.from_dicom(metadata)

            return DicomRepository.save(ct_image)
