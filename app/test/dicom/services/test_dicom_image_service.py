from unittest import TestCase, mock

import pydicom

from app.main.dicom.services.dicom_image_service import DicomImageService
from app.test import test_path


class TestImageService(TestCase):
    DICOM_IMAGE_PATH = f'{test_path}/resources/valid_dicom_image.dcm'

    def setUp(self) -> None:
        self.service = DicomImageService()
        self.dicom_file = pydicom.read_file(open(self.DICOM_IMAGE_PATH, 'rb'))

    @mock.patch('app.main.dicom.repositories.dicom_repository.DicomRepository.save')
    @mock.patch('pydicom.read_file')
    def test_read_dicom_image(self, pydicom_mock, repository):
        pydicom_mock.read_file.side_effect = self.dicom_file
        repository.save.side_effect = None

        file = open(self.DICOM_IMAGE_PATH, 'rb')

        self.service.create_from_dicom_file(file)

        pydicom_mock.assert_called_with(file)

    @mock.patch('pydicom.read_file')
    @mock.patch('app.main.dicom.repositories.dicom_repository.DicomRepository.save')
    def test_create_ct_image_from_file(self, repository, pydicom_mock):
        pydicom_mock.read_file.side_effect = self.dicom_file
        repository.save.side_effect = None

        file = open(self.DICOM_IMAGE_PATH, 'rb')

        self.service.create_from_dicom_file(file)

        repository.assert_called_once()
