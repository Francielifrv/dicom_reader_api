import os

from flask_mongoengine import MongoEngine

database = MongoEngine()


def init_app(app, environment):
    app.config['MONGO_URI'] = environment.MONGO_URI
    app.config['MONGOALCHEMY_DATABASE'] = 'computer_tomography_image'

    database.init_app(app)
