class Configuration:
    DEBUG = False
    MONGO_URI = 'mongodb:localhost:27017/computer_tomography_image'


class DevelopmentEnvironment(Configuration):
    DEBUG = True
    TESTING = True
    MONGODB_SETTINGS = {
        'db': 'computer_tomography_image',
        'host': 'localhost',
        'port': 27017
    }


class TestEnvironment(Configuration):
    DEBUG = True
    TESTING = True
    MONGO_URI = 'mongodb:localhost:27017/ct_image_test'
    MONGODB_SETTINGS = {
        'db': 'ct_image_test',
        'host': 'localhost',
        'port': 27017
    }


class ProductionEnvironment(Configuration):
    DEBUG = False
    TESTING = False
    MONGODB_PASSWORD = 'uAvpFTQUKD6nLm9w'
    MONGO_URI = f'mongodb+srv://dicom_user:{MONGODB_PASSWORD}@cluster0-k8hly.gcp.mongodb.net/computed_tomography_image'
    MONGODB_SETTINGS = {
        'db': 'computed_tomography_image',
        'host': MONGO_URI
    }

environment_configuration = {
    'dev': DevelopmentEnvironment,
    'test': TestEnvironment,
    'production': ProductionEnvironment
}


def get_environment_configuration_by_name(environment_name: str):
    return environment_configuration.get(environment_name)
