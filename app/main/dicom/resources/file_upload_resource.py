import json
from http import HTTPStatus

from flask import Flask
from flask_restplus import Resource, Api
from flask_restplus.reqparse import RequestParser
from pydicom.errors import InvalidDicomError
from werkzeug.datastructures import FileStorage
from werkzeug.exceptions import BadRequest

from app.main.dicom.services.dicom_image_service import DicomImageService

app = Flask(__name__)
api = Api(app=app)

ns = api.namespace('/', description='Uploads DICOM images and handle data')

DICOM_EXTENSION = '.dcm'

file_upload_parser = RequestParser()
file_upload_parser.add_argument('file', location='files', type=FileStorage, required=True)


@ns.route('/upload')
@ns.expect(file_upload_parser)
class FileUpload(Resource):

    @ns.response(HTTPStatus.CREATED, HTTPStatus.CREATED.value)
    @ns.doc('Upload DICOM image POST')
    def post(self):
        args = file_upload_parser.parse_args()
        file = args['file']

        if not self._is_dicom_file(file):
            raise BadRequest('Uploaded file must be a DICOM file.')

        service = DicomImageService()
        try:
            response = service.create_from_dicom_file(file)

            return app.response_class(
                response=json.dumps({'id': str(response.id)}),
                status=HTTPStatus.CREATED,
                mimetype='application/json')
        except InvalidDicomError as error:
            raise BadRequest(str(error))

    @staticmethod
    def _is_dicom_file(file):
        return file.filename.endswith(DICOM_EXTENSION)
