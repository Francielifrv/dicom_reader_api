from http import HTTPStatus

from flask import Flask
from flask_restplus import Api, Resource
from werkzeug.exceptions import NotFound

from app.main.dicom.models.ct_image import ComputedTomographyImage
from app.main.json_converter import to_json

app = Flask(__name__)
api = Api(app=app)

dicom_namespace = api.namespace('/', description='Handles dicom metadata')


@dicom_namespace.route('/dicom/metadata')
class DicomMetadata(Resource):

    @dicom_namespace.response(HTTPStatus.OK, HTTPStatus.OK.value)
    @dicom_namespace.doc('Retrieve DICOM image metadata')
    def get(self):
        query_result = ComputedTomographyImage.objects().to_json()

        return to_json(query_result)


@dicom_namespace.route('/dicom/metadata/<string:id>')
class DicomMetadataById(Resource):
    def get(self, id):
        query_result = ComputedTomographyImage.objects(id=id).first()

        if not query_result:
            raise NotFound(f'Cannot find metadata record with id {id}')

        return to_json(query_result.to_json())
