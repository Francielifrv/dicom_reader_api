from mongoengine import DateTimeField, StringField, IntField, EmbeddedDocumentField, EmbeddedDocument, ListField, \
    DecimalField, DateField, Document


class GeneralStudy(EmbeddedDocument):
    date_time = DateTimeField()
    study_id = IntField()
    study_instance_uid = StringField()
    referring_physician_name = StringField()
    accession_number = StringField()

    @classmethod
    def from_dicom(cls, dicom_image):
        return cls(study_id=dicom_image.get('StudyId'),
                   study_instance_uid=dicom_image.get('StudyInstanceUID'),
                   accession_number=dicom_image.get('AccessionNumber'))


class GeneralSeries(EmbeddedDocument):
    modality = StringField()
    series_number = StringField()
    series_date_time = StringField()
    series_description = StringField()
    series_instance_uuid = StringField()

    @classmethod
    def from_dicom(cls, dicom_image):
        return cls(modality=dicom_image.get('Modality'), series_number=dicom_image.get('SeriesNumber'),
                   series_description=dicom_image.get('SeriesDescriptionSeriesDescription'),
                   series_instance_uuid=dicom_image.get('SeriesInstanceUID'))


class ImagePlane(EmbeddedDocument):
    pixel_spacing = ListField()
    image_orientation_patient = ListField()
    image_position_patient = ListField()

    @classmethod
    def from_dicom(cls, dicom_image):
        return cls(pixel_spacing=dicom_image.get('PixelSpacing'),
                   image_orientation_patient=dicom_image.get('ImageOrientationPatient'),
                   image_position_patient=dicom_image.get('ImageOrientationPatient'))


class GeneralImage(EmbeddedDocument):
    instance_number = IntField()
    image_type = ListField()

    @classmethod
    def from_dicom(cls, dicom_image):
        return cls(instance_number=dicom_image.get('InstanceNumber'),
                   image_type=dicom_image.get('ImageType'))


class CTImage(EmbeddedDocument):
    bits_stored = IntField()
    bits_allocated = IntField()
    high_bit = IntField()
    rescale_intercept = DecimalField()
    rescale_slope = DecimalField()
    rescale_type = StringField()

    @classmethod
    def from_dicom(cls, dicom_image):
        return cls(bits_stored=dicom_image.get('BitsStored'),
                   bits_allocated=dicom_image.get('BitsAllocated'),
                   high_bit=dicom_image.get('HighBit'),
                   rescale_intercept=dicom_image.get('RescaleIntercept'),
                   rescale_slope=dicom_image.get('RescaleSlope'),
                   rescale_type=dicom_image.get('RescaleType'))


class SopCommon(EmbeddedDocument):
    sop_class_uid = StringField()
    sop_instance_uid = StringField()
    instance_creation_datetime = DateTimeField()
    instance_number = IntField()

    @classmethod
    def from_dicom(cls, dicom_image):
        return cls(sop_class_uid=dicom_image.get('SOPClassUID'),
                   sop_instance_uid=dicom_image.get('SOPInstanceUID'),
                   instance_number=dicom_image.get('InstanceNumber'))


class Patient(EmbeddedDocument):
    first_name = StringField()
    middle_name = StringField()
    family_name = StringField()
    patient_id = StringField()
    birth_date = DateField()
    sex = StringField()

    @classmethod
    def from_dicom(cls, dicom_image):
        patient_information = dicom_image.get('PatientName')
        gender = dicom_image.get('PatientSex')

        return cls(first_name=patient_information.given_name,
                   middle_name=patient_information.middle_name,
                   family_name=patient_information.family_name,
                   sex=gender)


class ComputedTomographyImage(Document):
    meta = {'allow_inheritance': True, 'abstract': False}

    patient = EmbeddedDocumentField(Patient)
    general_series = EmbeddedDocumentField(GeneralSeries)
    general_study = EmbeddedDocumentField(GeneralStudy)
    general_image = EmbeddedDocumentField(GeneralImage)
    image_plane = EmbeddedDocumentField(ImagePlane)
    ct_image = EmbeddedDocumentField(CTImage)
    sop_common = EmbeddedDocumentField(SopCommon)

    @classmethod
    def from_dicom(cls, dicom_image_ds):
        study = GeneralStudy.from_dicom(dicom_image_ds)

        patient = Patient.from_dicom(dicom_image_ds)

        image_plane = ImagePlane.from_dicom(dicom_image_ds)

        general_image = GeneralImage.from_dicom(dicom_image_ds)

        ct_image = CTImage.from_dicom(dicom_image_ds)

        sop_common = SopCommon.from_dicom(dicom_image_ds)

        general_series = GeneralSeries.from_dicom(dicom_image_ds)

        return cls(general_study=study, patient=patient, image_plane=image_plane, general_image=general_image,
                   ct_image=ct_image, sop_common=sop_common, general_series=general_series)
