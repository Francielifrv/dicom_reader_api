from flask import Flask, Blueprint
from flask_restplus import Api

import app.main.configuration.database as db
from app.main.configuration.environment import get_environment_configuration_by_name
from app.main.dicom.resources.dicom_resource import dicom_namespace
from app.main.dicom.resources.file_upload_resource import ns as file_upload_namespace

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='Dicom reader api',
          version='1.0',
          description='a boilerplate for flask restplus web service')

api.add_namespace(file_upload_namespace)
api.add_namespace(dicom_namespace)


def create_app(environment_name):
    app = Flask(__name__)

    configuration = get_environment_configuration_by_name(environment_name)
    app.config.from_object(configuration)
    app.register_blueprint(blueprint)

    db.init_app(app, configuration)

    app.app_context().push()

    return app
