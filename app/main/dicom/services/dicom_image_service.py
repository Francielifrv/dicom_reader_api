import pydicom

from app.main.dicom.models.ct_image import ComputedTomographyImage
from app.main.dicom.repositories.dicom_repository import DicomRepository


class DicomImageService():

    def create_from_dicom_file(self, file):
        dicom_image = pydicom.read_file(file)

        ct_image = ComputedTomographyImage.from_dicom(dicom_image)

        return DicomRepository.save(ct_image)
