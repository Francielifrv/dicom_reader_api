import json


def to_json(str_json):
    return json.loads(str_json, object_hook=remove_cls_node)


def remove_cls_node(data):
    formatted = {}

    for key, value in data.items():
        if key == '_id':
            formatted['id'] = str(value['$oid'])
        else:
            formatted[key] = value

    formatted.pop('_cls', None)
    return formatted
