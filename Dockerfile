FROM python:3.7.3-slim-stretch


RUN apt-get update && apt-get upgrade -yyq && apt-get install libpq-dev python3-dev gcc -yyq

RUN pip install poetry

WORKDIR /home/dicom_reader_api

COPY . .
RUN poetry config settings.virtualenvs.create false
RUN poetry install


EXPOSE 9091

ENTRYPOINT ["python", "manage.py", "run"]