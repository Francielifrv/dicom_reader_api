#Dicom Reader API

Reads dicom image metadata and stores it on mongodb database.

## Dependencies

* [python 3.7](https://www.python.org/downloads/release/python-373/)
* [Docker compose](https://docs.docker.com/compose/)
* [Google cloud SDK](https://cloud.google.com/sdk/)

## Runnning the application
From the project root execute:

1.  ```pip install -r requirements.txt``` to install the dependencies
2. ```docker-compose up``` to start the database
3. ```python manager.py run``` to startup the application

## Running the tests
Run `green app` on project root.*


_**Note**: Make sure the database is running when execting tests. Due to flask restplus restrictions, is not possible to write
proper unit tests for resources layer. Integration tests were added instead and those rely on database._

## Architectural decisions

The application is divided into resources. File uploads are made through ```/upload ``` resource and dicom metadata can be
accessed through `/metadata` sub resource. Swagger documentation for this API can be found on [this link](https://test-12349w839.appspot.com/).

The application currently does not store DICOM files locally or on the cloud, since the goal of this application is to 
process and display metadata. Store the files locally or on a cloud storage service would be a simple change though.

In order to avoid processing all DICOM files everytime all data needs to be retrieved, dicom metadata is stored on a database.
The metadata was organized accordingly to the data classification available for CT images on 
[inholitcs](https://dicom.innolitics.com/ciods/ct-image) website.

Mongodb was chosen as database since the relationship between metadata groups (eg.: patient information, study series, etc) 
represents one to one relationships. In this case, using one table to represent each object would create performance
issues due to the excessive amount of joins necessary to retrieve all the information.

A docker image is used to run mongodb, so it is not necessary to actually install mongo locally. This also make sure that
every local environment will use the same database version with the same configuration. On Google Cloud [mongodb cloud](https://www.mongodb.com/cloud)
is used instead, due to its simplicity and good compatibility with GC.