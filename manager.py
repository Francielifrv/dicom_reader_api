import os

from flask_script import Manager

from app.main.dicom import create_app

environment_name = os.getenv('ENV_NAME') or 'dev'

app = create_app(environment_name)
manager = Manager(app)


@manager.command
def run():
    app.run(host='0.0.0.0', port=9091)


if __name__ == '__main__':
    manager.run()
